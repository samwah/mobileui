import { NgModule }              from '@angular/core';
import { RouterModule, Routes }  from '@angular/router';
import { SymptomsComponent } from './symptoms/symptoms.component';
import { CategoriesComponent } from './categories/categories.component';
import { ResultComponent } from './result/result.component';

const appRoutes: Routes = [
    { path: 'categories', component: CategoriesComponent },
    { path: 'symptoms/:id', component: SymptomsComponent },
    { path: 'result', component: ResultComponent }

    ,
    {path: '',   redirectTo: '/categories', pathMatch: 'full' }];


    @NgModule({
        imports: [RouterModule.forRoot(appRoutes)],
        exports: [
          RouterModule
        ]
      })
      export class AppRoutingModule {}