import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { ElementFinder } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class FetcherService {

  url = 'assets/data.json';
  structure;
  private categories: object;
  private categoriesSubject: BehaviorSubject<object>;
  public categoriesObservable: Observable<object>;

  private patientSymptoms: object[]= [];
  private removedpatientSymptoms: object[]= [];

  private symptoms: object;
  private symptomsSubject: BehaviorSubject<object>;
  public symptomsObservable: Observable<object>;
  
  constructor(private http: HttpClient) {
    //this.getData().subscribe((data: Array<object>) => this.structure = data);
    
    this.categories = thedata.categories ;
    this.categoriesSubject = new BehaviorSubject(this.categories);
    this.categoriesObservable = this.categoriesSubject.asObservable();

    this.symptoms = thedata.symptoms;
    this.symptomsSubject = new BehaviorSubject(this.symptoms);
    this.symptomsObservable = this.symptomsSubject.asObservable();

    this.translateCategories('en');
    this.translateSymptoms('en');
  }
  getLanguages(){
    let languages: object[];
    languages = [
      {title: 'Suomalainen', iso:'fi'}, 
      {title: 'English', iso:'en'}, 
      {title: 'Svenska', iso:'sv'}
    ];
    return languages;
  }
  changeLanguage(isoLang:string){
    this.translateCategories(isoLang);
    this.translateSymptoms(isoLang);
  }

  translateCategories(isoLang:string){
    console.log(thedata);
    Object.keys(this.categories).forEach(key => {
      console.log(key);
      if (this.categories[key].type === "category") {
        this.categories[key].name = thedata.translations.categories[key][isoLang];
      } else if (this.categories[key].type === "symptom") {
        this.categories[key].name = thedata.translations.symptoms[key][isoLang];
      }
      console.log(this.categories[key].name);
    });

    this.categoriesSubject.next(this.categories);
    console.log(this.categories);
    return
  }
  translateSymptoms(isoLang: string){
    Object.keys(this.symptoms).forEach(key => {
      console.log(key);
      let tempKey = this.symptoms[key].translationID;
      if (tempKey === "standard") {
        //TODO implement backend translation
        //if translation is "standard" search with api

      } else {
        this.symptoms[key].name = thedata.translations.symptoms[tempKey][isoLang];
      }
    })
    
    this.symptomsSubject.next(this.symptoms);
    console.log(this.symptoms);
    return
  }
  getCategories(){
    return thedata.categoryList;
        //return this.http.get(this.url)
        //let temporaryData = this.data;
        //return temporaryData
  }

  addSymptom(symptom: any){
    //checkes if the symptoms in symptoms list not exists (to avoid double)
    if(!this.exists(symptom).exist){
      this.patientSymptoms.push(symptom);
     console.log("ADDED",this.patientSymptoms);
     for (var i=0; i< this.removedpatientSymptoms.length; i++){
      if(this.removedpatientSymptoms[i]["symptomId"] == symptom.symptomId){
          this.removedpatientSymptoms.splice(i,1);
          console.log("removed", symptom, "new list", this.patientSymptoms, "new list removed");

      }
    
        

      }
    }
  }
  getSymptoms(){
    return this.patientSymptoms;
  }
  getremovedSymptoms(){
    return this.removedpatientSymptoms;
  }
  removeSymptom(symptom: any){
    //checkes if the symptoms in symptoms list not exists then adds to removed list (in case no symptoms have been selected before)
    if(!this.exists(symptom).exist){
      this.removedpatientSymptoms.push(symptom);
     console.log("ADDED",this.patientSymptoms);
    }
    else{
    for (var i=0; i< this.patientSymptoms.length; i++){
      if(this.patientSymptoms[i]["symptomId"] == symptom.symptomId){
          this.patientSymptoms.splice(i,1);
          this.removedpatientSymptoms.push(symptom);
          console.log("removed", symptom, "new list", this.patientSymptoms, "new list removed");

      }
    
        

      }
    }
      //console.log("removed", symptom, "new list", this.patientSymptoms);
    }
  
  private exists(symptom){

    let existance = {
      exist: false,
      pos: -1
    };

    for(let i = 0; i < this.patientSymptoms.length; i++){
      if(this.patientSymptoms[i]["symptomId"] === symptom.symptomId){
        existance.exist = true;
        existance.pos = i;
      }
    }
    return existance;
  }

  
}


let thedata = {
    categoryList:[
      {categoryId: '111'},
      {categoryId: '222'},
      {categoryId: '333'},
    ],
    categories: {
        111:{
            name: "Loading",
            type: "category",
            list: [
              {type: "symptom", id: "11"},
              {type: "symptom", id: "12"},
              {type: "symptom", id: "13"},
              {type: "symptom", id: "14"},
              {type: "category", id: "3"},
              {type: "symptom", id: "20"},
              {type: "symptom", id: "21"}
            ]
        },
        3: {
          name: "Loading",
          type: "category",
          list: [
            {type: "symptom", id: "15"},
            {type: "symptom", id: "16"},
            {type: "symptom", id: "17"},
            {type: "symptom", id: "18"},
            {type: "symptom", id: "19"}
          ]
        },
        222:{
            name: "Loading",
            type: "symptom",
            symptomId: "7546",
            list: [
              {type: "symptom", id: "22"},
              {type: "symptom", id: "23"},
              {type: "symptom", id: "24"},
              {type: "symptom", id: "25"},
              {type: "symptom", id: "26"},
              {type: "category", id: "4"}
            ]
        },
        4: {
          name: "Loading",
          type: "symptom",
          symptomId: "2628",
          list: [
            {type: "symptom", id: "27"},
            {type: "symptom", id: "28"},
            {type: "symptom", id: "29"},
            {type: "symptom", id: "30"},
            {type: "symptom", id: "31"}
          ]
        },
        333: {
          name: "Loading",
          type: "category",
          list: [
            {type: "symptom", id: "11"},
            {type: "symptom", id: "12"},
            {type: "symptom", id: "13"},
            {type: "symptom", id: "14"},
            {type: "category", id: "3"},
            {type: "symptom", id: "20"},
            {type: "symptom", id: "21"}
          ]
      },
    },
    symptoms: {
      "11":{name: "Loading...", symptomId: "4218", custom: false, translationID: "1c0965cb-fdf6-48d9-b292-4f07f532ca0f"},
      "12":{name: "Loading...", symptomId: "6475", custom: false, translationID: "90a8b7d9-f2c1-4cee-a137-893f2a7d5b77"},
      "13":{name: "Loading...", symptomId: "6474", custom: false, translationID: "16018e9e-923e-45df-b232-e9c20c53869a"},
      "14":{name: "Loading...", symptomId: "4114", custom: false, translationID: "f7dd4016-80ef-49e8-bbe1-4944ca242fa2"},
      "15":{name: "Loading...", symptomId: "111568", custom: false, translationID: "standard"},
      "16":{name: "Loading...", symptomId: "7452", custom: false, translationID: "standard"},
      "17":{name: "Loading...", symptomId: "7866", custom: false, translationID: "standard"},
      "18":{name: "Loading...", symptomId: "8509", custom: false, translationID: "standard"},
      "19":{name: "Loading...", symptomId: "8166", custom: false, translationID: "standard"},
      "20":{name: "Loading...", symptomId: "10684", custom: false, translationID: "7f3cab9b-dfd2-4663-a765-39432442f436"},
      "21":{name: "Loading...", symptomId: "6119", custom: false, translationID: "standard"},
      "22":{name: "Loading...", symptomId: "4218", custom: false, translationID: "standard"},
      "23":{name: "Loading...", symptomId: "2431", custom: false, translationID: "standard"},
      "24":{name: "Loading...", symptomId: "8160", custom: false, translationID: "standard"},
      "25":{name: "Loading...", symptomId: "8162", custom: false, translationID: "standard"},
      "27":{name: "Loading...", symptomId: "2807", custom: false, translationID: "standard"},
      "28":{name: "Loading...", symptomId: "2807", custom: false, translationID: "standard"},
      "29":{name: "Loading...", symptomId: "2807", custom: false, translationID: "standard"},
      "30":{name: "Loading...", symptomId: "2807", custom: false, translationID: "standard"},
      "31":{name: "Loading...", symptomId: "2807", custom: false, translationID: "standard"},

    },
    translations:{
      categories:{
        3:{
          fi: 'Rintakipuja',
          en: 'Have you experienced dizziness?',
          sv: 'SV_Rintakipuja'
        },
        111:{
          fi: 'Flunssan oireita',
          en: 'Flu symptoms',
          sv: 'Influensasymptom'
        },
        333:{
          fi: 'Flunssan oireita3',
          en: 'Headache',
          sv: 'Influensasymptom3'
        }
    },
    symptoms:{
      222:{
        fi: 'Raajakipu',
        en: 'Dizziness',
        sv: 'SV_Raajakipu'
      },      
      4:{
        fi: 'Raajakipu',
        en: 'Dizziness',
        sv: 'SV_Raajakipu'
      },
      "1c0965cb-fdf6-48d9-b292-4f07f532ca0f":{
        fi: "Onko sinulla kuumetta?",
        en: "Do you have a fever?",
        sv: "Har du feber?"
      },
      "90a8b7d9-f2c1-4cee-a137-893f2a7d5b77":{
        fi: "Painetta poskionteloissa?",
        en: "Have you experiences shortness of breath?",
        sv: "SV_Painetta poskionteloissa?"
      },
      "16018e9e-923e-45df-b232-e9c20c53869a":{
        fi: "Painetta otsaonteloissa?",
        en: "Do you have sinus pressure?",
        sv: "SV_Painetta otsaonteloissa?"
      },
      "f7dd4016-80ef-49e8-bbe1-4944ca242fa2":{
        fi: "Kurkkukipua?",
        en: "Do you have a sore throat?",
        sv: "SV_Kurkkukipua?"
      },
      "7f3cab9b-dfd2-4663-a765-39432442f436":{
        fi: "Yskää?",
        en: "Do you have a runny or stuffy nose?",
        sv: "SV_Yskää?"
      },
      "4cc96a37-8797-427d-846c-f466d376c4a3":{
        fi: "Nuhaa?",
        en: "EN_Nuhaa?",
        sv: "SV_Nuhaa?"
      },
    }
  }
}

