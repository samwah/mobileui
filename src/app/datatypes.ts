export class Categories {
    constructor(
        public items: Template[]
    ) {
        
    }
}

export class Template {
    constructor(
        public name: string,
        public widget: string, //container
        public children: Question[]
    ) {
        this.name = name;
        this.widget = widget;
        this.children = children
    }
}


export class IntegerDetail {
    public min: number = undefined;
    public max: number = undefined;
    public def: number = undefined;
    public widget: string = undefined;

    constructor(
        public id: string, 
        public name: string, 
        public valuetype: string, //"int"
        min?: number,
        max?: number, 
        def?: number, 
        widget?: string
    ){
        this.min = min;
        this.max = max;
        this.def = def;
        this.widget = widget;
    }
}
export class StringDetail {
    public def: number = undefined;
    public widget: string = undefined;

    constructor(
        public id: string, 
        public name: string, 
        public valuetype: string, //"string"
        def?: number, 
        widget?: string
    ) {
        this.def = def;
        this.widget = widget;
    }
}
export class SelectDetail {
    public def: number = undefined;
    public widget: string = undefined;
    public multiple: boolean = undefined;
    
    constructor(
        public id: string,
        public name: string, 
        public valuetype: string, //"string", "int", "symptom" 
        public values: any[],  //string, number, SymptomValue 
        def? : number,
        widget?: string,
        multiple?: boolean
    ){
        this.def = def;
        this.widget = widget;
        this.multiple = multiple
    }
}
export class SymptomValue {
    constructor(
        public name:string, 
        public symptom: number
    ) {

    }
}
export class Question {
    public symptom: number = undefined;
    public details: any[] = undefined;
    public children: Question[] = undefined;

    constructor(
        public name: string,
        public widget: string, //yesno, container
        symptom?: number,
        details?: any[], //IntegerDetail, TextDetail, SelectDetail,
        children?: Question[]
    ) {
        this.symptom = symptom;
        this.details = details;
        this.children = children;
    }
}