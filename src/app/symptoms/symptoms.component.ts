import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {coerceNumberProperty} from '@angular/cdk/coercion';
import {ActivatedRoute} from '@angular/router';
import { FetcherService } from "../fetcher.service";

@Component({
  selector: 'app-symptoms',
  templateUrl: './symptoms.component.html',
  styleUrls: ['./symptoms.component.scss']
})
export class SymptomsComponent implements OnInit {

selectedsymptom: object;
selectedsymptoms: object[]= [];
removedsymptom: object;
disableMe: boolean = false;
textcolor: boolean = false;
isValid: boolean = false;
isRemoveButton: boolean = false;
  //Data from fetcherservice
  symptomsObject: object;
  categoriesObject: object;

 //Selected symptoms
  removedsymptomsArray: object[];
  symptomsArray: object[];
  categoriesArray: object[];

  //Data from categories / route
  currentCategory: string;

  autoTicks = false;
  disabled = false;
  invert = false;
  max = 100;
  min = 0;
  showTicks = false;
  step = 1;
  thumbLabel = false;
  value = 37;
  vertical = false;

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private _FetcherService: FetcherService) {
      this.route.params.subscribe(res => {
        this.currentCategory = res.id;
      });
      this.symptomsArray = _FetcherService.getSymptoms(); 
      this.removedsymptomsArray = _FetcherService.getremovedSymptoms(); 

      this._FetcherService.symptomsObservable.subscribe( data => this.symptomsObject = data);

      this._FetcherService.categoriesObservable.subscribe( data => {
        let fetchArray = [];
        let buildArray = [];

        this.categoriesObject = data; //All categories
        fetchArray = this.categoriesObject[this.currentCategory].list; //The selected category

        fetchArray.forEach(element => {
          if (element['type'] === 'symptom') {
            buildArray.push(this.symptomsObject[element['id']])
          } else if (element['type'] === 'category') {
            buildArray.push(this.categoriesObject[element['id']]); //this is the category
          }
        });
        this.categoriesArray = buildArray;

        //Object.keys(data).forEach(key =>{
        //});

      });

  }
  clickedyes(symptom){
    for (var i=0; i< this.symptomsArray.length; i++){
      if(this.symptomsArray[i]["symptomId"] === symptom.symptomId){
        //return this.symptomsArray[i]["symptomId"] === symptom.symptomId;
        return this.symptomsArray[i] === symptom;

      }
      

    }
    
  }
  clickedno(symptom){
    for (var i=0; i< this.removedsymptomsArray.length; i++){
      if(this.removedsymptomsArray[i]["symptomId"] == symptom.symptomId){
        //return this.symptomsArray[i]["symptomId"] === symptom.symptomId;
        return this.removedsymptomsArray[i] === symptom;

      }
      

    }
    
  }
	addSymptom(symptom){
    this._FetcherService.addSymptom(symptom);

  }
  removeSymptom(symptom){
    this._FetcherService.removeSymptom(symptom);
  }
  
  get tickInterval(): number | 'auto' {
    return this.showTicks ? (this.autoTicks ? 'auto' : this._tickInterval) : 0;
  }
  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
  private _tickInterval = 1;
  changeStyle(symptom): void{
    //colapsed = true and select syptom
    this.selectedsymptom = symptom;
    console.log("HÄR",this.selectedsymptom);
  }

  navigate(){
    this.router.navigate(['/result']);

  }
  back(){
    this.router.navigate(['']);
  }
  ngOnInit() {
    //Fetch id from categories
    this.route.params.subscribe(params => {
     
    });
  }

}
