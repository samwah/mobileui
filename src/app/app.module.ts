import {BrowserModule } from '@angular/platform-browser';
import {NgModule } from '@angular/core';

//navigation
import { AppRoutingModule } from './app-routing.module';
//import { RouterModule, Routes } from '@angular/router';
import { CategoriesRoutingModule} from './categories/categories-routing.module'
//import { Router } from '@angular/router';

//components
import {AppComponent } from './app.component';
import {SymptomsComponent } from './symptoms/symptoms.component';
import {CategoriesComponent } from './categories/categories.component';
import { HttpClientModule } from '@angular/common/http';
import {FetcherService}from './fetcher.service';

//material
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule, MatToolbarModule} from '@angular/material';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatDividerModule} from '@angular/material/divider';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatChipsModule} from '@angular/material/chips';
import {MatMenuModule} from '@angular/material/menu';

import { AngularSvgIconModule } from 'angular-svg-icon';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatSliderModule} from '@angular/material/slider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatInputModule} from '@angular/material';
import {MatRadioModule} from '@angular/material/radio';
import {MatCardModule} from '@angular/material/card';
import { ResultComponent } from './result/result.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PatientinfoComponent } from './patientinfo/patientinfo.component';



@NgModule({
  declarations: [
    AppComponent,
    SymptomsComponent,
    CategoriesComponent,
    ResultComponent,
    PatientinfoComponent,
  ],
  imports: [
    // RouterModule.forRoot(
    //   appRoutes,
    //   { enableTracing: true } // <-- debugging purposes only
    // ),
    // other imports here
    BrowserModule,
    CategoriesRoutingModule,
    AppRoutingModule,
    //RouterModule,
    HttpClientModule,
    AngularSvgIconModule,
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatToolbarModule,
    MatGridListModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    FlexLayoutModule,
    MatSliderModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatInputModule,
    MatRadioModule,
    MatCardModule,
    MatMenuModule,
    CategoriesRoutingModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatChipsModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [FetcherService],
  bootstrap: [AppComponent]
})
export class AppModule { 
  // constructor(router: Router) {
  //   console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
  // }
}
