import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FetcherService } from './fetcher.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],

})

export class AppComponent {
  languages: object[];
  constructor(
    private router: Router,
    private _FetcherService: FetcherService
  ){
    this.languages = _FetcherService.getLanguages();
  }
  changeLanguage(isoLang:string){
    this._FetcherService.changeLanguage(isoLang);
  }
}

