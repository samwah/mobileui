import {Component, OnInit } from '@angular/core';
import {Router } from '@angular/router';
import {FetcherService } from '../fetcher.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss'],

})

export class CategoriesComponent implements OnInit {

  selectedcategory: object;
  slider:boolean;
  categoryList: object[];
  categories : object;

  constructor(
    private router: Router,
    private _FetcherService:FetcherService
  ) {
    this.categoryList = _FetcherService.getCategories(); // Get top level category id's in array

    _FetcherService.categoriesObservable.subscribe(data => { // Get updated version of all categories
      this.categories = data;}
    );
  }
  
  onSelect(selection): void {
    this.selectedcategory = selection.categoryId;
    console.log(this.selectedcategory);
    this.navigate();
  }
  navigate() {
    this.router.navigate(['/symptoms', this.selectedcategory]);
    console.log("this is categoryid", this.selectedcategory);

}
  ngOnInit() {
  }

}
