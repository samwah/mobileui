import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SymptomsComponent } from '../symptoms/symptoms.component';

const categoriesRoutes: Routes = [
    { path: 'symptoms', component: SymptomsComponent },
];

@NgModule({
    imports: [
      RouterModule.forChild(categoriesRoutes)
    ],
    exports: [
      RouterModule
    ]
  })
  export class CategoriesRoutingModule { }