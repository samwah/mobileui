import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FetcherService } from "../fetcher.service";

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  symptomsArray: object[];

  constructor(private router: Router, private _FetcherService: FetcherService) {
    this.symptomsArray = _FetcherService.getSymptoms(); 

   }
  
  back(){
    this.router.navigate(['/symptoms']);
  }
  ngOnInit() {
  }

}
